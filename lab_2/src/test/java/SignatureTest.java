import org.bouncycastle.cms.CMSException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class SignatureTest {
    String testMessage = "test message 12345";
    String path_file_public = "\\big_data_analysis_systems_lab\\lab_2\\src\\test\\resources\\public.cer";
    String path_file_private = "\\big_data_analysis_systems_lab\\lab_2\\src\\test\\resources\\private.p12";

    @Test
    public void signatureTest(){
        Security.addProvider(new BouncyCastleProvider());
        CertificateFactory certFactory= null;
        try {
            certFactory = CertificateFactory
                    .getInstance("X.509", "BC");
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        X509Certificate certificate = null;
        try {
            certificate = (X509Certificate) certFactory
                    .generateCertificate(new FileInputStream(path_file_public));
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        char[] keystorePassword = "password".toCharArray();
        char[] keyPassword = "password".toCharArray();
        KeyStore keystore = null;
        try {
            keystore = KeyStore.getInstance("PKCS12");
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            keystore.load(new FileInputStream(path_file_private), keystorePassword);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        PrivateKey privateKey = null;
        try {
            privateKey = (PrivateKey) keystore.getKey("baeldung",
                    keyPassword);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }

        byte[] stringToEncrypt = testMessage.getBytes();
        byte[] encryptedData = new byte[0];
        try {
            encryptedData = Encryptor.encryptData(stringToEncrypt, certificate);
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        } catch (CMSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] rawData = new byte[0];
        try {
            rawData = Encryptor.decryptData(encryptedData, privateKey);
        } catch (CMSException e) {
            e.printStackTrace();
        }

        byte[] signedData = new byte[0];
        try {
            signedData = Signature.signData(rawData, certificate, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Boolean check = null;
        try {
            check = Signature.verifySignedData(signedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue(check);
    }

}
