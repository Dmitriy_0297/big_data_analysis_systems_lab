import org.bouncycastle.cms.CMSException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class EncryptorTest {
    String testMessage = "test message 12345";
    String path_file_public = "\\big_data_analysis_systems_lab\\lab_2\\src\\test\\resources\\public.cer";
    String path_file_private = "\\big_data_analysis_systems_lab\\lab_2\\src\\test\\resources\\private.p12";

    @Test
    public void encryptorTest() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        Security.addProvider(new BouncyCastleProvider());
        CertificateFactory certFactory = null;
        try {
            certFactory = CertificateFactory
                    .getInstance("X.509", "BC");
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        X509Certificate certificate = (X509Certificate) certFactory
                .generateCertificate(new FileInputStream(path_file_public));
        char[] keystorePassword = "password".toCharArray();
        char[] keyPassword = "password".toCharArray();
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream(path_file_private), keystorePassword);
        PrivateKey privateKey = (PrivateKey) keystore.getKey("baeldung",
                keyPassword);

        byte[] stringToEncrypt = testMessage.getBytes();
        byte[] encryptedData = new byte[0];
        try {
            encryptedData = Encryptor.encryptData(stringToEncrypt, certificate);
        } catch (CMSException e) {
            e.printStackTrace();
        }
        byte[] rawData = new byte[0];
        try {
            rawData = Encryptor.decryptData(encryptedData, privateKey);
        } catch (CMSException e) {
            e.printStackTrace();
        }
        String decryptedMessage = new String(rawData);
        Assert.assertEquals(decryptedMessage, testMessage);
    }
}
